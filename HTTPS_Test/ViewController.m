//
//  ViewController.m
//  HTTPS_Test
//
//  Created by 廖京辉 on 15/12/7.
//  Copyright © 2015年 廖京辉. All rights reserved.
//


/**
 *  1. 在 info.plist 文件里打开App Transport Security Settings
 *  2. 再打开 Exception Domains
 *  3. 将里面的baidu.com换成自己服务器的domian就可以了
 *
 */

#import "ViewController.h"

#define screen_height [UIScreen mainScreen].bounds.size.height
#define screen_width [UIScreen mainScreen].bounds.size.width
@interface ViewController ()

@end

@implementation ViewController{
    UIButton *afnetworking;
    UIButton *nsurlconnection;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpButton];
   }


-(void)setUpButton{
    afnetworking = [[UIButton alloc]init];
    afnetworking.frame = CGRectMake(screen_width*0.1, screen_height*0.5, screen_width*0.2, 30);
    [afnetworking setTitle:@"AFNetworking" forState:UIControlStateNormal];
    afnetworking.backgroundColor = [UIColor greenColor];
    [afnetworking addTarget:self action:@selector(afnetworkingAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:afnetworking];
    
    nsurlconnection = [[UIButton alloc]init];
    nsurlconnection.frame = CGRectMake(screen_width*0.7, screen_height*0.5, screen_width*0.2, 30);
    [nsurlconnection setTitle:@"NSURLConnection" forState:UIControlStateNormal];
    nsurlconnection.backgroundColor = [UIColor greenColor];
    [nsurlconnection addTarget:self action:@selector(nsurlconnectionAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nsurlconnection];

}
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] ==0){
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
    }else{
        [[challenge sender]cancelAuthenticationChallenge:challenge];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"error:%@",error);
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSLog(@"--->%@",response.accessibilityValue);
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSString *result = [[NSString alloc] initWithData:data  encoding:NSUTF8StringEncoding];
    NSLog(@"____>%@",result);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ButtonAction

-(void)afnetworkingAction{
    // 1.请求管理者
    AFHTTPRequestOperationManager *mgr = [AFHTTPRequestOperationManager manager];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
    securityPolicy.allowInvalidCertificates = YES;
    mgr.securityPolicy = securityPolicy;
    // 3.发送请求
    [mgr GET:@"https://baidu.com" parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"请求失败-%@", error);
    }];
}
-(void)nsurlconnectionAction{
    //    1.设置请求路径
    NSString *urlStr=[NSString stringWithFormat:@"https://www.baidu.com"];
    NSURL *url=[NSURL URLWithString:urlStr];
    //    2.创建请求对象
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    //    3.发送请求
    //发送同步请求，在主线程执行
    [NSURLConnection connectionWithRequest:request delegate:self];
}
@end
