//
//  ViewController.h
//  HTTPS_Test
//
//  Created by 廖京辉 on 15/12/7.
//  Copyright © 2015年 廖京辉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface ViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,NSURLConnectionDownloadDelegate>


@end

